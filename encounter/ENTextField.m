//
//  ENTextField.m
//  encounter
//
//  Created by Konstantin Beltikov on 04.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "ENTextField.h"

@implementation ENTextField

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:
                                      self.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
//        self.attributedPlaceholder =
//        [[NSAttributedString alloc] initWithString:@"ButtonText"
//                                        attributes:@{
//                                                     NSForegroundColorAttributeName: color,
//                                                     NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:17.0]
//                                                     }
//         ];
    }
    
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 10,
                      bounds.size.width - 10, bounds.size.height - 20);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    
    // Draw them with a 0.5 stroke width so they are a bit more visible.
    CGContextSetLineWidth(context, 0.3f);
    CGContextMoveToPoint(context, 0.0f + rect.size.width/10, rect.size.height-2); //start at this point
    CGContextAddLineToPoint(context, rect.size.width-rect.size.width/10, rect.size.height-2); //draw to this point
    
    // and now draw the Path!
    CGContextStrokePath(context);
}

@end

