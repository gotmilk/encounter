//
//  MenuTableViewController.m
//  encounter
//
//  Created by Konstantin Beltikov on 08.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "MenuTableViewController.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ENControl.h"

@interface MenuTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *messagesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UITableViewCell *logOutCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *settingsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *messagesCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *teamCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *infoCell;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@end

@implementation MenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    
    
    // Here we use the new provided setImageWithURL: method to load the web image
    [self.profileImg sd_setImageWithURL:[NSURL URLWithString:[ENControl sharedInstance].user.pic]
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  if (!error) {
                                      self.profileImg.image = image;
                                  } else {
                                      self.profileImg.image = nil;
                                      self.profileImg.backgroundColor = [UIColor redColor];
                                  }
    }];
    
    if ([ENControl sharedInstance].user.firstName && [ENControl sharedInstance].user.lastName) {
        self.userName.text = [NSString stringWithFormat:@"%@ %@", [ENControl sharedInstance].user.firstName, [ENControl sharedInstance].user.lastName];
    } else {
        self.userName.text = [NSString stringWithFormat:@"%@ (id %@)", [ENControl sharedInstance].user.username, [ENControl sharedInstance].user.userId];
    }
    
    
    
    // Or russian lang?
    NSString *rankStr = [ENParser getRankStr:[[ENControl sharedInstance].user.rank intValue]];
    
    self.rankLabel.text = [NSString stringWithFormat:@"%.1f / %@", [[ENControl sharedInstance].user.points floatValue], rankStr];
    
    
    if ([ENControl sharedInstance].newMessagesCount) {
        self.messagesLabel.text = [NSString stringWithFormat:@"%@ (%ld)", @"Messages", (long)[ENControl sharedInstance].newMessagesCount];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
////#warning Potentiall y incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
////#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    
    if (cell == self.logOutCell) {
        
        [[ENControl sharedInstance] logout:^(BOOL isComplete) {
            [self.mm_drawerController.navigationController popToRootViewControllerAnimated:YES];
        }];
    } else if (cell == self.infoCell) {
        NSLog(@"test");
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
