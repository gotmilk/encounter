//
//  WelcomeViewController.m
//  encounter
//
//  Created by Konstantin Beltikov on 03.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "WelcomeViewController.h"
#import <MMDrawerController/MMDrawerController.h>


@interface WelcomeViewController    ()
@property (weak, nonatomic) IBOutlet UIImageView *bg;

@end

@implementation WelcomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Check if user saved the keys
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (BOOL)shouldAutorotate
{
    if (UIInterfaceOrientationIsLandscape(self.navigationController.interfaceOrientation)) {
        return YES;
    }
    return NO;
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}
- (IBAction)showMenu:(id)sender {
    [self performSegueWithIdentifier:@"main" sender:self];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //     Get the new view controller using [segue destinationViewController].
    //     Pass the selected object to the new view controller.
//    if ([segue.identifier isEqualToString:@"main"]) {
//        NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:10];
//        //        [newArray addObject:[segue destinationViewController]];
//        self.navigationController.viewControllers = newArray;
//    }
    
    
//    if ([segue.identifier isEqualToString:@"main"]) {
//        NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:10];
//        //        [newArray addObject:[segue destinationViewController]];
//        self.navigationController.viewControllers = newArray;
//        
//        MMDrawerController *destinationViewController = (MMDrawerController *) segue.destinationViewController;
//        
//        [destinationViewController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
//        [destinationViewController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModePanningCenterView];
//        
//        // Instantitate and set the center view controller.
//        UIViewController *centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"center"];
//        [destinationViewController setCenterViewController:centerViewController];
//        
//        // Instantiate and set the left drawer controller.
//        UIViewController *leftDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menu"];
//        [destinationViewController setLeftDrawerViewController:leftDrawerViewController];
//        
//    }
}


@end
