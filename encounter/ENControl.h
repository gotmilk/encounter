//
//  ENControl.h
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ENUser.h"
#import "ENDomain.h"
#import "ENParser.h"
#import "ENMessage.h"

extern NSString *const loginURLMask;

@interface ENControl : NSObject

@property (nonatomic, strong) ENDomain *domain;
@property (nonatomic, strong) ENUser *user;

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;

@property (nonatomic, assign, getter=isAuthenticated) BOOL authenticated;

@property (nonatomic, assign) BOOL russian;

@property (nonatomic, assign) NSInteger newMessagesCount;



// Class
+ (ENControl *) sharedInstance;

// Authentication
- (void) login:(void (^)(BOOL isComplete, NSInteger errorCode, UIImage *guidImage, NSString *guidStr))complete withCode:(NSString *)code andGuid:(NSString *)guidMagic;
- (void) logout:(void (^)(BOOL isComplete))complete;
- (void) getMyInfo:(void (^)(ENUser *obj))complete;
- (void) getUserInfo:(NSString *)userId complete:(void (^)(ENUser *obj))complete;

// Registration
- (void) signup0:(void (^)(UIImage *image))complete;
- (void) signup1:(NSString *)login
        password:(NSString *)password
           email:(NSString *)email
           phone:(NSString *)phone
         country:(NSString *)country
    magicNumbers:(NSString *)magicNumbers
        complete:(void (^)(BOOL isComplete, NSInteger code))complete;

- (void) loadDomains:(void (^)(NSArray *sections, NSMutableDictionary *dic))complete;
- (void)downloadImage:(NSString *)imageURL complete:(void (^)(UIImage *image))completionBlock;
//- (void) update:(void (^)(BOOL isComplete))complete;
- (NSDictionary *) getCodes:(BOOL)russian;

// Load messages
//- (void) loadMessages:(void (^)(NSArray *, BOOL error))complete;
//- (void) sendMessage:(void (^)(ENMessage *, BOOL error))complete;
//- (void) getMessageById:(NSString *)messageId complete:(void (^)(ENMessage *, BOOL error))complete;

// Find users
//- (void) findUserByUsername:(NSString *)username complete:(void (^)(ENUser *, BOOL error))complete;
//- (void) findUserById:(NSString *)userId complete:(void (^)(ENUser *, BOOL error))complete;
//- (void) findTeamById:(NSString *)teamId complete:(void (^)(ENUser *, BOOL error))complete;


@end
