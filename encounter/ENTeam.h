//
//  ENTeam.h
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ENUser.h"

@class ENUser;

@interface ENTeam : NSObject
@property (nonatomic, strong) NSString *teamId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *points;
@property (nonatomic, strong) NSNumber *games;
@property (nonatomic, strong) NSNumber *wins;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, strong) NSString *forum;
@property (nonatomic, strong) ENUser *captain;
@property (nonatomic, strong) NSArray *active;
@property (nonatomic, strong) NSArray *passive;
@property (nonatomic, strong) NSArray *history;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *pic;

+ (ENTeam *)teamWithId:(NSString *)team_id;
@end
