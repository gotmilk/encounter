//
//  RoundImageView.m
//  encounter
//
//  Created by Konstantin Beltikov on 24.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "RoundImageView.h"

@implementation RoundImageView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self makeRounded];
        CGRect rect = self.frame;
        rect.size = CGSizeMake(46, 46);
        self.frame = rect;
        
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self makeRounded];
}

- (void)makeRounded
{
    NSLog(@"%f", self.bounds.size.width/2);
    self.layer.borderWidth = 0.0f;
    self.layer.cornerRadius = self.layer.bounds.size.width/2;
    self.layer.masksToBounds = YES;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
