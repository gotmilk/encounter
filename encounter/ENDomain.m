//
//  ENDomain.m
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "ENDomain.h"

@implementation ENDomain

+ (ENDomain *)domainWithAddress:(NSString *)domain
{
    ENDomain *obj = [[ENDomain alloc] init];
    [obj setAddress:domain];
    return obj;
}

@end
