//
//  CountryViewController.m
//  encounter
//
//  Created by Konstantin Beltikov on 05.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "CountryViewController.h"
#import "ENControl.h"
#import "RegisterViewController.h"

@interface CountryViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *keys;
@end

@implementation CountryViewController

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"countryCell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.f green:102/255.f blue:0.f alpha:1.f];
        [cell setSelectedBackgroundView:bgColorView];
        
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.textLabel setText:[self.keys objectAtIndex:indexPath.row]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    //...
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterViewController *reg = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
    reg.countryString = [self.keys objectAtIndex:indexPath.row];
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.keys count];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
        NSDictionary *dic = [[ENControl sharedInstance] getCodes:[ENControl sharedInstance].russian];
    
        self.keys = [[dic allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
    
//        self.keys = [[[ENControl sharedInstance] getCodes:[ENControl sharedInstance].russian] keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2) {
////            if ([obj1 isKindOfClass:[NSString class]] && [obj2 isKindOfClass:[NSString class]]) {
//                return [(NSString *)obj1 compare:(NSString *)obj2];
////            }
////            return 0;
//        }];
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
