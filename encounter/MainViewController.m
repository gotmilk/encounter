//
//  MainViewController.m
//  encounter
//
//  Created by Konstantin Beltikov on 03.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "MainViewController.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>

@interface MainViewController () {
//    MMDrawerController * drawerController;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu;

@end

@implementation MainViewController

- (IBAction)showLeft:(id)sender {
    
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        }];
}

-(void)viewDidLoad
{
//    UIViewController * leftDrawer = [[UIViewController alloc] init];
//    UIViewController * center = [[UIViewController alloc] init];
//    UIViewController * rightDrawer = [[UIViewController alloc] init];
//    
//    drawerController = [[MMDrawerController alloc]
//                                             initWithCenterViewController:center
//                                             leftDrawerViewController:leftDrawer
//                                             rightDrawerViewController:rightDrawer];
}

@end
