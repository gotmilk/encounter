//
//  ENParser.h
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ENDomain.h"
#import "ENUser.h"

// Error codes for auth
#define AUTH_OK                 0
#define AUTH_ERR_WRONG_LOGIN    1
#define AUTH_ERR_NOT_ACTIVATED  2
#define AUTH_ERR_WRONG_DOMAIN   3

// Error codes
#define ERR_SUCCESS 0
#define ERR_UNKNOWN 1

#define ERR_NO_LOGIN    2
#define ERR_NO_EMAIL    3
#define ERR_NO_PASSWORD 4
#define ERR_NO_CODE     5

#define ERR_WRONG_PASS_SAME     6
#define ERR_WRONG_PASS_LENGTH   7
#define ERR_WRONG_PASS_SYMBOLS  8
#define ERR_WRONG_LOGIN_EXISTS  9
#define ERR_WRONG_LOGIN_LENGTH  10
#define ERR_WRONG_EMAIL_EXISTS  11
#define ERR_WRONG_CODE          12
#define ERR_WRONG_EMAIL         13

#define ERR_TOO_MANY_ATTEMPTS   14

@interface ENParser : NSObject

@property (nonatomic, strong) NSString *domain;

+ (id)initWithDomain:(NSString *)domain;
- (NSInteger) parseAuthentication:(NSData *)content;
- (ENUser *) parseUserInfo:(NSData *)content;
- (NSString *) parseSignUp:(NSData *)content;
- (NSInteger) parseSignUpResult:(NSData *)content;
- (BOOL) parseDomains:(NSData *)content complete:(void (^)(NSArray *sections, NSMutableDictionary *dic))complete;
+ (NSString *) getRankStr:(NSInteger)rank;
- (NSInteger) getMessagesCount:(NSData *)content;
@end
