//
//  DomainViewController.m
//  encounter
//
//  Created by Konstantin Beltikov on 05.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "DomainViewController.h"
#import "ENControl.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "RegisterViewController.h"

@interface DomainViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *keys;
@property (nonatomic, strong) NSDictionary *dic;
@end

@implementation DomainViewController

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"domainCell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.f green:102/255.f blue:0.f alpha:1.f];
        [cell setSelectedBackgroundView:bgColorView];
        
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    
    NSString *sectionName = [self.keys objectAtIndex:indexPath.section];
    NSArray *secArray = [self.dic objectForKey:sectionName];
    NSString *titleText = [secArray objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:titleText];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    //...
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionName = [self.keys objectAtIndex:section];
    NSArray *secArray = [self.dic objectForKey:sectionName];
    
    return [secArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.keys count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName = [self.keys objectAtIndex:section];
    
    return sectionName;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterViewController *reg = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
    
    NSString *sectionName = [self.keys objectAtIndex:indexPath.section];
    NSArray *secArray = [self.dic objectForKey:sectionName];
    NSString *titleText = [secArray objectAtIndex:indexPath.row];
    
    reg.domainString = titleText;
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    MBProgressHUD *mb = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    //    mb.color = [UIColor grayColor];
    //    mb.labelText = @"Loading";
    mb.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.f alpha:0.8f];
//    [self.view addSubview:mb];
    [self.navigationController.view addSubview:mb];
    [mb show:YES];

    [[ENControl sharedInstance] loadDomains:^(NSArray *sections, NSMutableDictionary *dic) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [mb hide:YES];
        if (sections) {
            self.keys = sections;
            self.dic = dic;
            [self.tableView reloadData];
            NSLog(@"loading");
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Error happend while loading the list!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
