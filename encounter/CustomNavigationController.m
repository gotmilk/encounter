//
//  CustomNavigationController.m
//  encounter
//
//  Created by Konstantin Beltikov on 03.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "CustomNavigationController.h"

@implementation CustomNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationBar setBarTintColor:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:1.f]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,
      [UIColor whiteColor], NSForegroundColorAttributeName,
      [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"Helvetica" size:21.0], NSFontAttributeName,
      nil]];
    
    // Hide bottom shadow
    //    UIImageView * navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationBar];
    //    [navBarHairlineImageView setHidden:YES];
//    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    
    
    // Set background color
    self.navigationBar.translucent = NO;
}

- (BOOL)shouldAutorotate
{
    return [self.visibleViewController shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.visibleViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [self.visibleViewController preferredInterfaceOrientationForPresentation];
}

@end
