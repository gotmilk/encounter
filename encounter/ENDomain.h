//
//  ENDomain.h
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ENUser.h"

@class ENUser;

@interface ENDomain : NSObject
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) ENUser *owner;
@property (nonatomic, strong) NSArray *admins;

+ (ENDomain *)domainWithAddress:(NSString *)domain;

@end
