//
//  ENButton.m
//  encounter
//
//  Created by Konstantin Beltikov on 04.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "ENButton.h"

@implementation ENButton


//@synthesize backgroundStates;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.layer.borderWidth = 1.2f;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.cornerRadius = 4.5f;
        
//        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [self setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
//        [self setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [self setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
    
    return self;
}

//- (void) setBackgroundColor:(UIColor *) _backgroundColor forState:(UIControlState) _state {
//    if (backgroundStates == nil)
//        backgroundStates = [[NSMutableDictionary alloc] init];
//    
//    [backgroundStates setObject:_backgroundColor forKey:[NSNumber numberWithInt:_state]];
//    
//    if (self.backgroundColor == nil)
//        [self setBackgroundColor:_backgroundColor];
//}
//
//- (UIColor*) backgroundColorForState:(UIControlState) _state {
//    return [backgroundStates objectForKey:[NSNumber numberWithInt:_state]];
//}
//
//#pragma mark -
//#pragma mark Touches
//
//- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    [super touchesBegan:touches withEvent:event];
//    
//    UIColor *selectedColor = [backgroundStates objectForKey:[NSNumber numberWithInt:UIControlStateHighlighted]];
//    if (selectedColor) {
//        CATransition *animation = [CATransition animation];
//        [animation setType:kCATransitionFade];
//        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//        [self.layer addAnimation:animation forKey:@"EaseOut"];
//        self.backgroundColor = selectedColor;
//    }
//}
//
//- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
//    [super touchesCancelled:touches withEvent:event];
//    
//    UIColor *normalColor = [backgroundStates objectForKey:[NSNumber numberWithInt:UIControlStateNormal]];
//    if (normalColor) {
//        CATransition *animation = [CATransition animation];
//        [animation setType:kCATransitionFade];
//        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//        [self.layer addAnimation:animation forKey:@"EaseOut"];
//        self.backgroundColor = normalColor;
//    }
//}
//
//- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    [super touchesEnded:touches withEvent:event];
//    
//    UIColor *normalColor = [backgroundStates objectForKey:[NSNumber numberWithInt:UIControlStateNormal]];
//    if (normalColor) {
//        CATransition *animation = [CATransition animation];
//        [animation setType:kCATransitionFade];
//        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//        [self.layer addAnimation:animation forKey:@"EaseOut"];
//        self.backgroundColor = normalColor;
//    }
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
