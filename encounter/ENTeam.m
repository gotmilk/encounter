//
//  ENTeam.m
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "ENTeam.h"

@implementation ENTeam

+ (ENTeam *)teamWithId:(NSString *)team_id
{
    ENTeam *obj = [[ENTeam alloc] init];
    
    if (obj) {
        obj.teamId = team_id;
    }
    
    return obj;
}

@end
