//
//  RegisterViewController.m
//  encounter
//
//  Created by Konstantin Beltikov on 03.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "RegisterViewController.h"
#import "ENControl.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface RegisterViewController () {
    NSString *lastDomain;
}
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UIButton *selectCountry;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *selectDomain;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UIImageView *capchaImage;
@end

@implementation RegisterViewController


- (IBAction)refreshCapcha:(id)sender
{
    if (self.domainString) {
        
        [ENControl sharedInstance].domain = [ENDomain domainWithAddress:self.domainString];
        MBProgressHUD *mb = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        //    mb.color = [UIColor grayColor];
        //    mb.labelText = @"Loading";
        mb.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.f alpha:0.8f];
        [self.navigationController.view addSubview:mb];
        [mb show:YES];
        
        [[ENControl sharedInstance] signup0:^(UIImage *image) {
            [mb hide:YES];
            self.capchaImage.image = image;
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.capchaImage.layer.borderColor = [UIColor colorWithRed:0.5f green:0.5f blue:0.5f alpha:0.4f].CGColor;
    self.capchaImage.layer.borderWidth = 1.f;
    self.capchaImage.layer.cornerRadius = 9.f;
    
    if (self.domainString) {
        [ENControl sharedInstance].domain = [ENDomain domainWithAddress:self.domainString];
        [self.selectDomain setTitle:self.domainString forState:UIControlStateNormal];
    }
    
    if (self.countryString) {
        [self.selectCountry setTitle:self.countryString forState:UIControlStateNormal];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (self.domainString && ![lastDomain isEqualToString:self.domainString]) {
        
        lastDomain = self.domainString;
        [ENControl sharedInstance].domain = [ENDomain domainWithAddress:self.domainString];
        MBProgressHUD *mb = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    //    mb.color = [UIColor grayColor];
    //    mb.labelText = @"Loading";
        mb.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.f alpha:0.8f];
        [self.navigationController.view addSubview:mb];
        [mb show:YES];
    
        [[ENControl sharedInstance] signup0:^(UIImage *image) {
            [mb hide:YES];
            self.capchaImage.image = image;
        }];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    [self.scrollView setContentOffset:CGPointMake(0.f, contentInsets.bottom)];
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
//    if (!CGRectContainsPoint(aRect, self.view.frame) ) {
//        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
//        [self.scrollView setContentOffset:scrollPoint animated:YES];
//    }
    
    
    
//    // self.scrollView.contentSize needs to be set appropriately
//    CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
////    NSTimeInterval duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    
//    
//    CGRect visibleArea = self.view.frame;
//    visibleArea.size.height -= keyboardRect.size.height;
//    
//    CGPoint scrollPoint = CGPointZero;
//    CGPoint visiblePoint = CGPointMake(0, visibleArea.size.height);
//    
//    CGRect frame = CGRectInset(self.view.frame, 0, CGRectGetHeight(keyboardRect));
//    self.scrollView.frame = frame;
//    
////    CGRectInset(self.scrollView.frame, 0, CGRectGetHeight(keyboardRect));
////    
////    if (!CGRectContainsPoint(visibleArea, visiblePoint)) {
////        CGFloat a = visiblePoint.y - keyboardRect.origin.y;
////        scrollPoint = CGPointMake(0.0, a);
////    }
//    
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, scrollPoint.y, 0.0);
//    
////    [UIView beginAnimations:nil context:NULL];
////    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
////    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
////    [UIView setAnimationBeginsFromCurrentState:YES];
////    
////    self.scrollView.contentInset = contentInsets;
////    self.scrollView.scrollIndicatorInsets = contentInsets;
//    self.scrollView.contentOffset = scrollPoint;
//    
//    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.loginTextField) {
        [self.passTextField becomeFirstResponder];
        return NO;
    } else if (textField == self.passTextField) {
        [self.emailTextField becomeFirstResponder];
        return NO;
    } else if (textField == self.emailTextField) {
        [self.phoneTextField becomeFirstResponder];
        return NO;
    } else if (textField == self.phoneTextField) {
        [self.codeTextField becomeFirstResponder];
        return NO;
    } else if (textField == self.codeTextField) {
        [textField resignFirstResponder];
        [self registerStart];
        return YES;
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

// Vinum/stllfckngdd123
- (void) registerStart
{
    MBProgressHUD *mb = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    //    mb.color = [UIColor grayColor];
    //    mb.labelText = @"Loading";
    mb.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.f alpha:0.8f];
    [self.navigationController.view addSubview:mb];
    [mb show:YES];
    
    [[ENControl sharedInstance] signup1:self.loginTextField.text password:self.passTextField.text email:self.emailTextField.text phone:self.phoneTextField.text country:self.countryString magicNumbers:self.codeTextField.text complete:^(BOOL isComplete, NSInteger code) {
        [mb hide:YES];
        if (isComplete) {
            [self performSegueWithIdentifier:@"verification" sender:self];
        } else {
            NSString *errMsg = nil;
            
                switch (code) {
                    case ERR_NO_LOGIN:
                        errMsg = @"Please, enter your login!";
                        break;
                    
                    case ERR_NO_EMAIL:
                        errMsg = @"Please, enter your email!";
                        break;
                        
                    case ERR_NO_PASSWORD:
                        errMsg = @"Please, enter your password!";
                        break;
                        
                    case ERR_NO_CODE:
                        errMsg = @"Please, enter verification code!";
                        break;
                    
                    case ERR_WRONG_CODE:
                        errMsg = @"Wrong verification code!";
                        break;
                        
                    case ERR_WRONG_LOGIN_EXISTS:
                        errMsg = @"That username is already in use!";
                        break;
                        
                    case ERR_WRONG_LOGIN_LENGTH:
                        errMsg = @"Wrong login length, login length should be > 3 and < 32!";
                        break;
                        
                    case ERR_WRONG_EMAIL_EXISTS:
                        errMsg = @"That email is already exists in database!";
                        break;
                        
                    case ERR_WRONG_PASS_LENGTH:
                        errMsg = @"Wrong password length, password length should be > 6 and < 32!";
                        break;
                        
                    case ERR_WRONG_EMAIL:
                        errMsg = @"Your email is incorrect!";
                        break;
                        
                    case ERR_WRONG_PASS_SAME:
                        errMsg = @"Your password is equal to username!";
                        break;
                        
                    case ERR_WRONG_PASS_SYMBOLS:
                        errMsg = @"You password should contain at least one letter and one digit.";
                        break;
                        
                    case ERR_UNKNOWN:
                        errMsg = @"Unknown error. Please, check your data!";
                        break;
                        
                    default:
                        errMsg = @"Something went wrong! Please try again";
                        break;
                }
                
                [[[UIAlertView alloc] initWithTitle:@"Error!" message:errMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            
            [self refreshCapcha:nil];
        }
    }];
}
- (IBAction)registerButton:(id)sender
{
    [self.view endEditing:YES];
    [self registerStart];
}

- (BOOL)shouldAutorotate
{
    if (UIInterfaceOrientationIsLandscape(self.navigationController.interfaceOrientation)) {
        return YES;
    }
    return NO;
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //     Get the new view controller using [segue destinationViewController].
    //     Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"verification"]) {
        
        [ENControl sharedInstance].username = self.loginTextField.text;
        [ENControl sharedInstance].password = self.passTextField.text;
        
        NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:10];
        //        [newArray addObject:[segue destinationViewController]];
        self.navigationController.viewControllers = newArray;
    }
}

@end
