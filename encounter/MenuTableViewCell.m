//
//  MenuTableViewCell.m
//  encounter
//
//  Created by Konstantin Beltikov on 23.02.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
