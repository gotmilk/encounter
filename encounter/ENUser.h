//
//  ENUser.h
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ENDomain.h"
#import "ENTeam.h"

@class ENDomain;
@class ENTeam;

@interface ENUser : NSObject
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) ENDomain *domain;
@property (nonatomic, strong) NSNumber *points;
@property (nonatomic, strong) NSNumber *rank;
@property (nonatomic, strong) ENTeam *team;
@property (nonatomic, assign) BOOL isCapitan;
@property (nonatomic, strong) NSString *location;

@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *gender;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *patronymicName;
@property (nonatomic, strong) NSString *lastName;

//@property (nonatomic, assign) NSInteger maritalStatus;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *icq;
@property (nonatomic, strong) NSString *skype;
@property (nonatomic, strong) NSString *pic;


@end
