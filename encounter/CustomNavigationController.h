//
//  CustomNavigationController.h
//  encounter
//
//  Created by Konstantin Beltikov on 03.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController
@end
