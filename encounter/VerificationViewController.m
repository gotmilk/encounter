//
//  VerificationViewController.m
//  encounter
//
//  Created by Konstantin Beltikov on 06.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "VerificationViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "ENControl.h"
#import <MMDrawerController/MMDrawerController.h>
#import <MMDrawerController/MMDrawerVisualState.h>

@interface VerificationViewController ()

@end

@implementation VerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)reloadVerification:(id)sender {
    [self loginStart];
}

// Vinum/stllfckngdd123
- (void)loginStart
{
    MBProgressHUD *mb = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    //    mb.color = [UIColor grayColor];
    //    mb.labelText = @"Loading";
    mb.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.f alpha:0.8f];
    [self.navigationController.view addSubview:mb];
    [mb show:YES];
    
    [[ENControl sharedInstance] login:^(BOOL isComplete, NSInteger errorCode, UIImage *guid, NSString *guidStr) {
        [mb hide:YES];
        if (isComplete) {
            switch (errorCode) {
                case 0:
                    [self performSegueWithIdentifier:@"main" sender:self];
                    break;
                    
            }
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Connection error!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    } withCode:nil andGuid:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//     Get the new view controller using [segue destinationViewController].
//     Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"main"]) {
        //        NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:10];
        //        [newArray addObject:[segue destinationViewController]];
        //        self.navigationController.viewControllers = newArray;
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
        MMDrawerController *destinationViewController = (MMDrawerController *) segue.destinationViewController;
        
        
        [destinationViewController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        destinationViewController.closeDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
        
        // Instantitate and set the center view controller.
        UIViewController *centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"center"];
        [destinationViewController setCenterViewController:centerViewController];
        
        // Instantiate and set the left drawer controller.
        UIViewController *leftDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menu"];
        [destinationViewController setLeftDrawerViewController:leftDrawerViewController];
        //        destinationViewController.animationVelocity = 0.2f;
        destinationViewController.showsShadow = NO;
        //        destinationViewController.
        //        [destinationViewController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
        
        [destinationViewController setDrawerVisualStateBlock:[MMDrawerVisualState parallaxVisualStateBlockWithParallaxFactor:20]];
        
        //        destinationViewController.showsStatusBarBackgroundView = YES;
        //        destinationViewController.statusBarViewBackgroundColor = [UIColor whiteColor];
        //
        //        [newArray addObject:destinationViewController];
        
        //        [destinationViewController setMaximumLeftDrawerWidth:200.];
        
        //        [destinationViewController
        //         setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
        //             MMDrawerControllerDrawerVisualStateBlock block;
        ////             block = [[MMExampleDrawerVisualStateManager sharedManager]
        ////                      drawerVisualStateBlockForDrawerSide:drawerSide];
        ////             if(block){
        ////                 block(drawerController, drawerSide, percentVisible);
        ////             }
        //         }];
    }
}


@end
