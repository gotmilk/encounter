//
//  LoginViewController.m
//  encounter
//
//  Created by Konstantin Beltikov on 03.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>
#import "LoginViewController.h"
#import "ENControl.h"
#import <MMDrawerController/MMDrawerController.h>
#import <MMDrawerController/MMDrawerVisualState.h>
#import "ENTextField.h"
#import "ENButton.h"

@interface LoginViewController () {
    NSString *saveGUID;
    CGFloat defaultConst;
}
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UIView *capchaView;
@property (weak, nonatomic) IBOutlet ENButton *loginButton;
@property (weak, nonatomic) IBOutlet ENTextField *codeTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *capchaViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *capchaImageView;
@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.capchaView.hidden = YES;
    defaultConst = self.capchaViewHeight.constant;
    self.capchaViewHeight.constant -= 153.f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
//    [self.userTextField becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
//    [self.scrollView setContentOffset:CGPointMake(0.f, contentInsets.bottom)];
    self.scrollView.scrollIndicatorInsets = contentInsets;

//    self.scrollView.frame
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
//    CGRect aRect = self.view.frame;
//    aRect.size.height -= kbSize.height;
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    [UIView commitAnimations];
}

// Vinum/stllfckngdd123
- (void)loginStart
{
    MBProgressHUD *mb = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
//    mb.color = [UIColor grayColor];
//    mb.labelText = @"Loading";
    mb.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.f alpha:0.8f];
    [self.navigationController.view addSubview:mb];
    [mb show:YES];
    
    [ENControl sharedInstance].username = self.userTextField.text;
    [ENControl sharedInstance].password = self.passTextField.text;
    
    [[ENControl sharedInstance] login:^(BOOL isComplete, NSInteger errorCode, UIImage *guidImage, NSString *guidStr) {
        [mb hide:YES];
        if (isComplete) {
            switch (errorCode) {
                case 0:
                     [self performSegueWithIdentifier:@"main" sender:self];
                    break;
                    
                case 2:
                    [self performSegueWithIdentifier:@"verification" sender:self];
                    break;
                
                case 1:
                    [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Please check your login/password!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    break;
                    
                default:
                    [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Wrong username/password!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    break;
            }
        } else {
            if (errorCode == ERR_TOO_MANY_ATTEMPTS) {
                saveGUID = guidStr;
                self.capchaView.hidden = NO;
                self.capchaImageView.image = guidImage;
                self.capchaViewHeight.constant = defaultConst;
                NSLog(@"default %f", defaultConst);
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Connection error!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
        }
    } withCode:self.capchaView.hidden?nil:self.codeTextField.text andGuid:self.capchaView.hidden?nil:saveGUID];
}
- (IBAction)loginButton:(id)sender
{
    [self.view endEditing:YES];
    [self loginStart];
//    self.capchaViewHeight.constant = 50.1f;
}
//
//- (BOOL)shouldAutorotate
//{
//    if (UIInterfaceOrientationIsLandscape(self.navigationController.interfaceOrientation)) {
//        return YES;
//    }
//    return NO;
//}
//
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
//
//- (NSUInteger) supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientationPortrait;
//}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender { 
    //     Get the new view controller using [segue destinationViewController].
    //     Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"main"]) {
//        NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:10];
        //        [newArray addObject:[segue destinationViewController]];
//        self.navigationController.viewControllers = newArray;
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
        MMDrawerController *destinationViewController = (MMDrawerController *) segue.destinationViewController;
        
        
        [destinationViewController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        destinationViewController.closeDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
        
        // Instantitate and set the center view controller.
        UIViewController *centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"center"];
        [destinationViewController setCenterViewController:centerViewController];
        
        // Instantiate and set the left drawer controller.
        UIViewController *leftDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menu"];
        [destinationViewController setLeftDrawerViewController:leftDrawerViewController];
//        destinationViewController.animationVelocity = 0.2f;
        destinationViewController.showsShadow = NO;
//        destinationViewController.
//        [destinationViewController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
        
        [destinationViewController setDrawerVisualStateBlock:[MMDrawerVisualState parallaxVisualStateBlockWithParallaxFactor:20]];
        
//        destinationViewController.showsStatusBarBackgroundView = YES;
//        destinationViewController.statusBarViewBackgroundColor = [UIColor whiteColor];
//
//        [newArray addObject:destinationViewController];
        
//        [destinationViewController setMaximumLeftDrawerWidth:200.];
        
//        [destinationViewController
//         setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
//             MMDrawerControllerDrawerVisualStateBlock block;
////             block = [[MMExampleDrawerVisualStateManager sharedManager]
////                      drawerVisualStateBlockForDrawerSide:drawerSide];
////             if(block){
////                 block(drawerController, drawerSide, percentVisible);
////             }
//         }];
    }
}

@end
