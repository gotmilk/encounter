//
//  ENControl.m
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "ENControl.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking.h>

/*
    Masks for paths
 */
NSString *const loginURLMask = @"%@Login.aspx?lang=ru&return=/UserDetails.aspx?lang=ru";
NSString *const myInfoURLMask = @"%@UserDetails.aspx?lang=ru";
NSString *const userInfoURLMask = @"%@UserDetails.aspx?lang=ru&uid=%@";
NSString *const signUpURLMask = @"%@SignUp.aspx?lang=ru";
NSString *const capchaURLMask = @"%@MagicKey.aspx?guid=%@";
NSString *const domainsListURLMask = @"%@Geography.aspx?choice=all&lang=%@";

@interface ENControl()
{
    NSString *BaseURLString;
    AFHTTPRequestOperationManager *manager;
    NSString *guid;
    ENParser *parser;
}
@end

@implementation ENControl

@synthesize user = _user, domain = _domain;

+ (ENControl *) sharedInstance
{
    static dispatch_once_t p = 0;
    __strong static ENControl *_sharedObject = nil;
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
        _sharedObject.authenticated = NO;
    });
    return _sharedObject;
}

- (instancetype) init
{
    self = [super init];
    
    if (self) {
        manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager.operationQueue setMaxConcurrentOperationCount:20];
        
        manager.requestSerializer.timeoutInterval = 10.f;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        parser = [[ENParser alloc] init];
    }
    
    return self;
}

//- (void) setUser:(ENUser *)user
//{
//    _user = user;
//    BaseURLString = [NSString stringWithFormat:@"http://%@/", user.domain.address];
//}

- (void) setDomain:(ENDomain *)domain
{
    _domain = domain;
    BaseURLString = [NSString stringWithFormat:@"http://%@/", domain.address];
}


- (void) logout:(void (^)(BOOL))complete
{
    // Clear cookies
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
    complete(YES);
}

- (void) login:(void (^)(BOOL isComplete, NSInteger errorCode, UIImage *guidImage, NSString *guidStr))complete withCode:(NSString *)code andGuid:(NSString *)guidMagic
{
    BaseURLString = @"http://world.en.cx/";
    NSString *loginDomain = [NSString stringWithFormat:@"%@", BaseURLString];
    NSString *loginPage = [NSString stringWithFormat:loginURLMask, loginDomain];
    
    // Clear cookies
    NSHTTPCookie *cookie;
    
//    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    for (cookie in [storage cookies]) {
//        [storage deleteCookie:cookie];
//    }
    
    // Get users subdomain
    if (![ENControl sharedInstance].username || ![ENControl sharedInstance].password) {
        complete(NO, 0, nil, nil);
        return;
    }
    
    // TODO: Social Assign !?
    // Vinum/stllfckngdd123
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"socialAssign": @"0",
                                                                                      @"Login": [ENControl sharedInstance].username,
                                                                                      @"Password": [ENControl sharedInstance].password,
                                                                                      @"EnButton1": @"Вход",
                                                                                      @"ddlNetwork": @"0"}];
    if (code && guidMagic) {
        [parameters setValue:guidMagic forKey:@"MagicGuid"];
        [parameters setValue:code forKey:@"MagicNumbers"];
    }
    
    
    // Set cookie to disable mobile
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                @".en.cx", NSHTTPCookieDomain,
                                @"/", NSHTTPCookiePath,
                                @"mobile", NSHTTPCookieName,
                                @"0", NSHTTPCookieValue,
                                nil];
    
    cookie = [NSHTTPCookie cookieWithProperties:properties];
////    NSLog(@"cookie: %@", cookie);
    [cookieStorage setCookie: cookie];
    
    properties = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                  @".quest.ua", NSHTTPCookieDomain,
                  @"/", NSHTTPCookiePath,
                  @"mobile", NSHTTPCookieName,
                  @"0", NSHTTPCookieValue,
                  nil];
    cookie = [NSHTTPCookie cookieWithProperties:properties];
//    NSLog(@"cookie: %@", cookie);
    [cookieStorage setCookie: cookie];
    

    [manager POST:loginPage parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *guidBuf = [parser parseSignUp:responseObject];
        
        if (guidBuf) {
            NSString *capchaImage = [NSString stringWithFormat:capchaURLMask, loginDomain, guidBuf];
            [self downloadImage:capchaImage complete:^(UIImage *image) {
                complete(NO, ERR_TOO_MANY_ATTEMPTS, [self convertToGreyscale:image], guidBuf);
            }];
            return;
        }
        
        NSInteger errorCode = [parser parseAuthentication:responseObject];
        if (!errorCode) {
            /*
                Save users data
             */
            self.user = [parser parseUserInfo:responseObject];
            self.newMessagesCount = [parser getMessagesCount:responseObject];
            
            /*
                Save cookies, create cookie for domain quest
             */
            
            NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:@"http://world.en.cx/"]];
            for (NSHTTPCookie *cook in cookies) {
                if ([cook.name isEqualToString:@"atoken"]) {
                    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                @".quest.ua", NSHTTPCookieDomain,
                                                @"/", NSHTTPCookiePath,
                                                @"atoken", NSHTTPCookieName,
                                                cook.value, NSHTTPCookieValue,
                                                [NSNumber numberWithBool:NO], NSHTTPCookieSecure,
                                                @"0", NSHTTPCookieVersion,
                                                nil];
                    [properties setObject:cook.expiresDate forKey:NSHTTPCookieExpires];
                    [cookieStorage setCookie: [NSHTTPCookie cookieWithProperties:properties]];
                    break;
                }
            }
            complete(YES, 0, nil, nil);
        } else {
            complete(YES, errorCode, nil, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        complete(NO, 0, nil, nil);
    }];
}

- (void) getMyInfo:(void (^)(ENUser *obj))complete
{
    [[ENControl sharedInstance] getUserInfo:nil complete:^(ENUser *obj) {
        complete(obj);
    }];
}

- (void) getUserInfo:(NSString *)userId complete:(void (^)(ENUser *obj))complete
{
    NSString *userInfoPage;
    
    if (userId) {
        userInfoPage = [NSString stringWithFormat:userInfoURLMask, BaseURLString, userId];
    } else {
        userInfoPage = [NSString stringWithFormat:myInfoURLMask, BaseURLString];
    }
    
    [manager GET:userInfoPage parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        complete([parser parseUserInfo:responseObject]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        complete(nil);
    }];
}

- (NSDictionary *) getCodes:(BOOL)russian
{
    
    if (!russian) {
        NSDictionary *dic = @{@"Armenia +374": @"100006",
                              @"Australia +61": @"100001",
                              @"Austria +43": @"100002",
                              @"Azerbaijan +994": @"100003",
                              @"Belarus +375": @"100007",
                              @"Bulgaria +359": @"100011",
                              @"Canada +1": @"100032",
                              @"China +86": @"100035",
                              @"Czech Republic +420": @"100065",
                              @"Egypt +20": @"100023",
                              @"Estonia +372": @"100068",
                              @"Finland +358": @"100063",
                              @"France +33": @"100064",
                              @"Georgia +995": @"100021",
                              @"Germany +49": @"100018",
                              @"Great Britain +44": @"100013",
                              @"Greece +30": @"100020",
                              @"Hungary +36": @"100014",
                              @"Israel +972": @"100024",
                              @"Italy +39": @"100029",
                              @"Kazakhstan +7": @"100030",
                              @"Kyrgyzstan +996": @"100034",
                              @"Latvia +371": @"100038",
                              @"Lithuania +370": @"100040",
                              @"Moldova, Republic Of +373": @"100043",
                              @"Netherlands +31": @"100019",
                              @"Norway +47": @"100046",
                              @"Poland +48": @"100047",
                              @"Romania +40": @"100072",
                              @"Russian Federation +7": @"100050",
                              @"Senegal +221": @"100135",
                              @"Spain +34": @"100028",
                              @"Sweden +46": @"100067",
                              @"Switzerland +41": @"100066",
                              @"Thailand +66": @"100092",
                              @"Tunisia +216": @"100122",
                              @"Turkey +90": @"100059",
                              @"Ukraine +380": @"100062",
                              @"United Arab Emirates +971": @"100093",
                              @"United States +1": @"100055",
                              @"Uzbekistan +998": @"100061",
                              @"Абхазия +7": @"100301",
                              @"Доминиканская республика +1": @"100138",
                              @"Индонезия +62": @"100074",
                              @"Куба +53": @"100113",
                              @"Afghanistan +93": @"100144",
                              @"Albania +355": @"100114",
                              @"Algeria +213": @"100098",
                              @"Andorra +376": @"100145",
                              @"Angola +244": @"100119",
                              @"Antigua And Barbuda +1": @"100147",
                              @"Antilles, The +599": @"100207",
                              @"Argentina +54": @"100005",
                              @"Bahamas, The +1": @"100080",
                              @"Bahrain +973": @"100127",
                              @"Bangladesh +880": @"100149",
                              @"Barbados +1": @"100128",
                              @"Belgium +32": @"100009",
                              @"Belize  +501": @"100008",
                              @"Bolivia +591": @"100123",
                              @"Burkina Faso +226": @"100156",
                              @"Burma +95": @"100205",
                              @"Cambodia +855": @"100158",
                              @"Central African Republic +236": @"100129",
                              @"Chad +235": @"100130",
                              @"Chile +56": @"100081",
                              @"Colombia +57": @"100082",
                              @"Croatia +385": @"100071",
                              @"Ecuador +593": @"100103",
                              @"El Salvador +503": @"100051",
                              @"Equatorial Guinea +240": @"100172",
                              @"Eritrea +291": @"100173",
                              @"Ethiopia +251": @"100121",
                              @"Faroe Islands +298": @"100175",
                              @"Fiji +679": @"100176",
                              @"French Guinea +594": @"100177",
                              @"French Polynesia +689": @"100178",
                              @"Guatemala +502": @"100185",
                              @"Holy Land +379": @"100191",
                              @"Jamaica +1": @"100132",
                              @"Japan +81": @"100070",
                              @"Macau +853": @"100199",
                              @"Madagascar +261": @"100134",
                              @"Malawi +265": @"100125",
                              @"Malaysia +60": @"100076",
                              @"Maldives +960": @"100200",
                              @"Mali +223": @"100133",
                              @"Malta +356": @"100086",
                              @"Martinique +596": @"100201",
                              @"Mauritania +222": @"100108",
                              @"Mauritius +230": @"100202",
                              @"Mayotte +269": @"100203",
                              @"Mexico +52": @"100042",
                              @"Monaco +377": @"100044",
                              @"Mongolia +976": @"100139",
                              @"Montenegro +382": @"100247",
                              @"Montserrat +1": @"100204",
                              @"Morocco +212": @"100104",
                              @"Mozambique +258": @"100117",
                              @"Namibia +264": @"100102",
                              @"Nauru +674": @"100206",
                              @"Nepal +977": @"100107",
                              @"New Caledonia +687": @"100208",
                              @"New Zealand +64": @"100045",
                              @"Nicaragua +505": @"100209",
                              @"Niger +227": @"100210",
                              @"Nigeria +234": @"100115",
                              @"Niue +683": @"100211",
                              @"North Korea +850": @"100084",
                              @"Oman +968": @"100213",
                              @"Pakistan +92": @"100087",
                              @"Panama +507": @"100124",
                              @"Papua New Guinea +675": @"100088",
                              @"Paraguay +595": @"100110",
                              @"Peru +51": @"100089",
                              @"Philippines +63": @"100090",
                              @"Portugal +351": @"100048",
                              @"Puerto Rico +1": @"100246",
                              @"Rwanda +250": @"100217",
                              @"Saint Helena +290": @"100218",
                              @"Saint Kitts and Nevis +1": @"100219",
                              @"Saint Pierre and Miquelon +508": @"100221",
                              @"Saint Vincent and the Grenadines +1": @"100222",
                              @"Samoa +685": @"100223",
                              @"San Marino +378": @"100224",
                              @"Sao Tome and Principe +239": @"100225",
                              @"Saudi Arabia +966": @"100091",
                              @"Serbia +381": @"100226",
                              @"Seychelles +248": @"100109",
                              @"Sierra Leone +232": @"100227",
                              @"Singapore +65": @"100077",
                              @"Slovakia +421": @"100052",
                              @"Slovenia +386": @"100053",
                              @"Solomon Islands +677": @"100228",
                              @"Somalia +252": @"100229",
                              @"South Africa +27": @"100141",
                              @"South Korea +82": @"100069",
                              @"Sri Lanka +94": @"100120",
                              @"St. Lucia Island +1": @"100220",
                              @"Sudan +249": @"100232",
                              @"Suriname +597": @"100054",
                              @"Swaziland +268": @"100234",
                              @"Syria +963": @"100106",
                              @"Taiwan +886": @"100078",
                              @"Tajikistan +992": @"100056",
                              @"Tanzania +255": @"100101",
                              @"Togo +228": @"100136",
                              @"Tokelau +690": @"100235",
                              @"Tonga, Kingdom of +676": @"100236",
                              @"Transnistria +373": @"100302",
                              @"Trinidad and Tobago +1": @"100237",
                              @"Turkmenistan +993": @"100057",
                              @"Turks and Caicos Islands +1": @"100058",
                              @"Tuvalu +688": @"100239",
                              @"Uganda +256": @"100060",
                              @"Uruguay +598": @"100111",
                              @"Zimbabwe +263": @"100096",
                              @"Ангуилья +1": @"100004",
                              @"Арулько +297": @"100142",
                              @"Бенин +229": @"100151",
                              @"Бермуды +1": @"100010",
                              @"Босния/Герцеговина +387": @"100079",
                              @"Ботсвана +267": @"100100",
                              @"Бразилия +55": @"100012",
                              @"Британская Океания +1": @"100153",
                              @"Бруней +673": @"100155",
                              @"Бурунди +257": @"100157",
                              @"Бутан +975": @"100152",
                              @"Валлис и Футуна о-ва +681": @"100241",
                              @"Вануату +678": @"100240",
                              @"Венесуэла +58": @"100095",
                              @"Вьетнам +84": @"100015",
                              @"Габон +241": @"100180",
                              @"Гаити +509": @"100016",
                              @"Гайана +592": @"100189",
                              @"Гамбия +220": @"100181",
                              @"Гана +233": @"100105",
                              @"Гваделупа +590": @"100017",
                              @"Гвинея +224": @"100187",
                              @"Гвинея-Бисау +245": @"100188",
                              @"Гибралтар +350": @"100143",
                              @"Гондурас +504": @"100137",
                              @"Гонконг +852": @"100073",
                              @"Гренада +1": @"100184",
                              @"Гренландия +299": @"100094",
                              @"Д.Р. Конго +243": @"100244",
                              @"Дания +45": @"100022",
                              @"Джибути +253": @"100169",
                              @"Доминика +1": @"100170",
                              @"Еретриа +291": @"100245",
                              @"Замбия +260": @"100116",
                              @"Западная Сахара +212": @"100242",
                              @"Индия +91": @"100025",
                              @"Иордания +962": @"100075",
                              @"Ирак +964": @"100140",
                              @"Иран +98": @"100026",
                              @"Ирландия +354": @"100027",
                              @"Исландия +354": @"100083",
                              @"Йемен +967": @"100243",
                              @"Кабо-Верде +238": @"100159",
                              @"Кайманские о-ва +1": @"100160",
                              @"Камерун +237": @"100031",
                              @"Катар +974": @"100216",
                              @"Кения +254": @"100097",
                              @"Кипр +357": @"100033",
                              @"Кирибати +686": @"100195",
                              @"Кокосовы (Килинг) о-ва +61891": @"100163",
                              @"Коморские о-ва +269": @"100164",
                              @"Конго (Brazzaville) +242": @"100112",
                              @"Конго (Kinshasa) +243": @"100165",
                              @"Коста-Рика +506": @"100036",
                              @"Кот-д'Ивуар +225": @"100168",
                              @"Кувейт +965": @"100037",
                              @"Кука о-ва +682": @"100166",
                              @"Лаос +856": @"100196",
                              @"Лесото +266": @"100197",
                              @"Либерия +231": @"100198",
                              @"Ливан +961": @"100099",
                              @"Ливия +218": @"100039",
                              @"Лихтенштейн +423": @"100126",
                              @"Люксембург +352": @"100041",
                              };
        
        return dic;
    } else {
        NSDictionary *dic = @{@"Австралия +61":@"100001",
                              @"Австрия +43":@"100002",
                              @"Азербайджан +994":@"100003",
                              @"Албания +355":@"100114",
                              @"Алжир +21":@"100098",
                              @"Ангола +244":@"100119",
                              @"Андорра +376":@"100145",
                              @"Антигуа и Барбуда +1268":@"100147",
                              @"Аргентина +54":@"100005",
                              @"Армения +374":@"100006",
                              @"Афганистан +93":@"100144",
                              @"Бангладеш +880":@"100149",
                              @"Барбадос +1246":@"100128",
                              @"Бахрейн +973":@"100127",
                              @"Белиз +501":@"100008",
                              @"Белоруссия +375":@"100007",
                              @"Бельгия +32":@"100009",
                              @"Бенин +229":@"100151",
                              @"Болгария +359":@"100011",
                              @"Боливия +591":@"100123",
                              @"Ботсвана +267":@"100100",
                              @"Бразилия +55":@"100012",
                              @"Бруней +673":@"100155",
                              @"Буркина Фасо +226":@"100156",
                              @"Бурунди +257":@"100157",
                              @"Бутан +975":@"100152",
                              @"Вануату +678":@"100240",
                              @"Великобритания +44":@"100013",
                              @"Венгрия +36":@"100014",
                              @"Венесуэла +58":@"100095",
                              @"Вьетнам +84":@"100015",
                              @"Габон +241":@"100180",
                              @"Гаити +509":@"100016",
                              @"Гайана +592":@"100189",
                              @"Гамбия +220":@"100181",
                              @"Гана +233":@"100105",
                              @"Гватемала +502":@"100185",
                              @"Гвинея +224":@"100187",
                              @"Германия +49":@"100018",
                              @"Гибралтар +350":@"100143",
                              @"Гондурас +504":@"100137",
                              @"Гонконг +852":@"100073",
                              @"Гренада +1-473":@"100184",
                              @"Гренландия +299":@"100094",
                              @"Греция +30":@"100020",
                              @"Грузия +995":@"100021",
                              @"Дания +45":@"100022",
                              @"Джибути +253":@"100169",
                              @"Доминика +1767":@"100170",
                              @"Доминиканская республика +1809":@"100138",
                              @"Египет +20":@"100023",
                              @"Замбия +260":@"100116",
                              @"Западная Сахара +21":@"100242",
                              @"Зимбабве +263":@"100096",
                              @"Израиль +972":@"100024",
                              @"Индия +91":@"100025",
                              @"Индонезия +62":@"100074",
                              @"Иордания +962":@"100075",
                              @"Ирак +964":@"100140",
                              @"Иран +98":@"100026",
                              @"Ирландия +353":@"100027",
                              @"Исландия +354":@"100083",
                              @"Испания +34":@"100028",
                              @"Италия +39":@"100029",
                              @"Казахстан +7":@"100030",
                              @"Камбоджа +855":@"100158",
                              @"Камерун +237":@"100031",
                              @"Катар +974":@"100216",
                              @"Кения +254":@"100097",
                              @"Кипр +357":@"100033",
                              @"Кирибати +686":@"100195",
                              @"Китай +86":@"100035",
                              @"Колумбия +57":@"100082",
                              @"Коморские о-ва +269":@"100164",
                              @"Конго +242":@"100244",
                              @"Куба +53":@"100113",
                              @"Кувейт +965":@"100037",
                              @"Лаос +856":@"100196",
                              @"Латвия +371":@"100038",
                              @"Либерия +231":@"100198",
                              @"Ливан +961":@"100099",
                              @"Ливия +21":@"100039",
                              @"Литва +370":@"100040",
                              @"Лихтенштейн +41":@"100126",
                              @"Люксембург +352":@"100041",
                              @"Маврикий +230":@"100202",
                              @"Мавритания +222":@"100108",
                              @"Мадагаскар +261":@"100134",
                              @"Макао +853":@"100199",
                              @"Малави +265":@"100125",
                              @"Малайзия +60":@"100076",
                              @"Мали +223":@"100133",
                              @"Мальдивские о-ва +960":@"100200",
                              @"Мальта +356":@"100086",
                              @"Мексика +52":@"100042",
                              @"Мозамбик +258":@"100117",
                              @"Монако +377":@"100044",
                              @"Монголия +976":@"100139",
                              @"Намибия +264":@"100102",
                              @"Науру +674":@"100206",
                              @"Непал +977":@"100107",
                              @"Нигер +227":@"100210",
                              @"Нигерия +234":@"100115",
                              @"Нидерланды +31":@"100019",
                              @"Никарагуа +505":@"100209",
                              @"Новая Зеландия +64":@"100045",
                              @"Норвегия +47":@"100046",
                              @"Оман +968":@"100213",
                              @"Пакистан +92":@"100087",
                              @"Панама +507":@"100124",
                              @"Папуа Новая Гвинея +675":@"100088",
                              @"Парагвай +595":@"100110",
                              @"Перу +51":@"100089",
                              @"Польша +48":@"100047",
                              @"Португалия +351":@"100048",
                              @"Пуэрто Рико +1787":@"100246",
                              @"Россия +7":@"100050",
                              @"Руанда +250":@"100217",
                              @"Румыния +40":@"100072",
                              @"Сальвадор +503":@"100051",
                              @"Саудовская Аравия +966":@"100091",
                              @"Свазиленд +268":@"100234",
                              @"Северная Корея +850":@"100084",
                              @"Сенегал +221":@"100135",
                              @"Сингапур +65":@"100077",
                              @"Сирия +963":@"100106",
                              @"Словакия +421":@"100052",
                              @"Словения +386":@"100053",
                              @"Соломоновы о-ва +677":@"100228",
                              @"Сомали +252":@"100229",
                              @"Судан +249":@"100232",
                              @"Суринам +597":@"100054",
                              @"США +1":@"100055",
                              @"Таджикистан +992":@"100056",
                              @"Тайвань +886":@"100078",
                              @"Танзания +255":@"100101",
                              @"Того +228":@"100136",
                              @"Тринидад и Тобаго +1868":@"100237",
                              @"Тувалу +688":@"100239",
                              @"Тунис +21":@"100122",
                              @"Туркменистан +993":@"100057",
                              @"Турция +90":@"100059",
                              @"Уганда +256":@"100060",
                              @"Узбекистан +998":@"100061",
                              @"Украина +380":@"100062",
                              @"Уругвай +598":@"100111",
                              @"Фарерские о-ва +298":@"100175",
                              @"Фиджи +679":@"100176",
                              @"Финляндия +358":@"100063",
                              @"Франция +33":@"100064",
                              @"Хорватия +385":@"100071",
                              @"Чад +235":@"100130",
                              @"Чехия +420":@"100065",
                              @"Чили +56":@"100081",
                              @"Швейцария +41":@"100066",
                              @"Швеция +46":@"100067",
                              @"Эквадор +593":@"100103",
                              @"Экваториальная Гвинея +240":@"100172",
                              @"Эстония +372":@"100068",
                              @"Эфиопия +251":@"100121",
                              @"ЮАР +27":@"100141",
                              @"Южная Корея +82":@"100069",
                              @"Ямайка +1876":@"100132",
                              @"Япония +81":@"100070"
                              };
        return dic;
    }
}



- (void) downloadImage:(NSString *)imageURL complete:(void (^)(UIImage *image))completionBlock
{
    NSString *signUpPage = [NSString stringWithFormat:signUpURLMask, BaseURLString];
    [manager.requestSerializer setValue:signUpPage forHTTPHeaderField:@"Referer"];
    
    [manager GET:imageURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        UIImage *image = [[UIImage alloc] initWithData:responseObject];
        completionBlock(image);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completionBlock(nil);
    }];
}

- (void) loadDomains:(void (^)(NSArray *sections, NSMutableDictionary *dic))complete
{
    [self setDomain:[ENDomain domainWithAddress:@"world.en.cx"]];
    NSString *domainsPage = [NSString stringWithFormat:domainsListURLMask, BaseURLString, [ENControl sharedInstance].russian?@"ru":@"en"];
    
    [manager GET:domainsPage  parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [parser parseDomains:responseObject complete:^(NSArray *sections, NSMutableDictionary *dic) {
            complete(sections, dic);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        complete(nil, nil);
    }];
}

- (void) signup0:(void (^)(UIImage *image))complete
{
    NSString *userInfoPage = [NSString stringWithFormat:signUpURLMask, BaseURLString];
    
    if (![ENControl sharedInstance].domain) {
        NSLog(@"Error: no domain found");
        complete(nil);
        return;
    }
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
    [manager GET:userInfoPage parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        guid = [parser parseSignUp:responseObject];
        if (guid) {
            NSString *capchaImage = [NSString stringWithFormat:capchaURLMask, BaseURLString, guid];
            [self downloadImage:capchaImage complete:^(UIImage *image) {
                complete([self convertToGreyscale:image]);
            }];
        } else {
            complete(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        complete(nil);
    }];
}

- (void) signup1:(NSString *)login
       password:(NSString *)password
          email:(NSString *)email
          phone:(NSString *)phone
        country:(NSString *)country
    magicNumbers:(NSString *)magicNumbers
       complete:(void (^)(BOOL isComplete, NSInteger code))complete
{
    NSString *signupPage = [NSString stringWithFormat:signUpURLMask, BaseURLString];
    
    /*
     hdSimpleForm:
     hdGUID:
     Login:my_login
     Password:my_login1
     Password2:my_login1
     CountryCodes:100050
     Phone:1234567890
     Email:test@asdasd.ru
     MagicGuid:0b62217e-1410-4eef-84d0-07554a9b56ea
     MagicNumbers:870209
     RegistrationForm$SignUpButton.x:31
     RegistrationForm$SignUpButton.y:12
     RegistrationForm$SignUpButton:SignUp
     */
    
    if (!guid || !country || !magicNumbers || !phone || !email || !password) {
        complete(NO, 0);
        return;
    }
    
    
    NSString *countryCode = [[self getCodes:[ENControl sharedInstance].russian] valueForKey:country];
    
    if (!countryCode) {
        complete(NO, 0);
        return;
    }
    
    NSDictionary *parameters = @{@"hdSimpleForm": @"",
                                 @"hdGUID": @"",
                                 @"Login": login,
                                 @"Password": password,
                                 @"Password2": password,
                                 @"CountryCodes": countryCode,
                                 @"Phone": phone,
                                 @"Email": email,
                                 @"MagicGuid": guid,
                                 @"MagicNumbers": magicNumbers,
                                 @"RegistrationForm$SignUpButton.x": @"47",
                                 @"RegistrationForm$SignUpButton.y": @"13",
                                 @"RegistrationForm$SignUpButton": @"SignUp",
                                };

    
    // Set domain for parser
    parser.domain = [ENControl sharedInstance].domain.address;
    
    // TODO: Social Assign !?
    [manager POST:signupPage parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // parse
        NSInteger code = [parser parseSignUpResult:responseObject];
        if (!code) {
            NSLog(@"Registred!");
            complete(YES, 0);
        } else {
            NSLog(@"Err!");
            complete(NO, code);
        }
        NSLog(@"RESULT: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        complete(NO, 0);
    }];
}

- (UIImage *) convertToGreyscale:(UIImage *)i {
    
    int kRed = 1;
    int kGreen = 2;
    int kBlue = 4;
    
    int colors = kGreen | kBlue | kRed;
    int m_width = i.size.width;
    int m_height = i.size.height;
    
    uint32_t *rgbImage = (uint32_t *) malloc(m_width * m_height * sizeof(uint32_t));
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImage, m_width, m_height, 8, m_width * 4, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, NO);
    CGContextDrawImage(context, CGRectMake(0, 0, m_width, m_height), [i CGImage]);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // now convert to grayscale
    uint8_t *m_imageData = (uint8_t *) malloc(m_width * m_height);
    for(int y = 0; y < m_height; y++) {
        for(int x = 0; x < m_width; x++) {
            uint32_t rgbPixel=rgbImage[y*m_width+x];
            uint32_t sum=0,count=0;
            
            if (((rgbPixel>>16)&255) > 120 && ((rgbPixel>>24)&255) > 120) {
                if (colors & kRed) {sum += (rgbPixel>>24)&255; count++;}
                if (colors & kGreen) {sum += (rgbPixel>>16)&255; count++;}
                if (colors & kBlue) {sum += (rgbPixel>>8)&255; count++;}
            } else {
                count++;
            }
            m_imageData[y*m_width+x]=sum/count;
        }
    }
    free(rgbImage);
    
    // convert from a gray scale image back into a UIImage
    uint8_t *result = (uint8_t *) calloc(m_width * m_height *sizeof(uint32_t), 1);
    
    // process the image back to rgb
    for(int i = 0; i < m_height * m_width; i++) {
        result[i*4]=0;
        int val=m_imageData[i];
        result[i*4+1]=val;
        result[i*4+2]=val;
        result[i*4+3]=val;
    }
    
    // create a UIImage
    colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(result, m_width, m_height, 8, m_width * sizeof(uint32_t), colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGImageRef image = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    
    free(m_imageData);
    
    // make sure the data will be released by giving it to an autoreleased NSData
    [NSData dataWithBytesNoCopy:result length:m_width * m_height];
    
    return resultUIImage;
}


@end
