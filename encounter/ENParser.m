//
//  ENParser.m
//  encounter
//
//  Created by Konstantin Beltikov on 01.01.15.
//  Copyright (c) 2015 smedialink. All rights reserved.
//

#import "ENParser.h"

@implementation ENParser

+ (id)initWithDomain:(NSString *)domain
{
    ENParser *obj = [[ENParser alloc] init];
    [obj setDomain:domain];
    return obj;
}

- (NSInteger) parseAuthentication:(NSData *)content
{
    NSString *logoutMarker = @"Login.aspx?action=logout";
    NSString *encounterMarker = @"Encounter Ltd. All rights reserved";
    NSString *verifyAccount = @"<h1 class=\"PageTitle\">Подтверждение E-Mail</h1>";
    NSString *dataStr = [[NSString alloc] initWithData:content encoding:NSUTF8StringEncoding];
    
    
    if ([dataStr rangeOfString:encounterMarker].location == NSNotFound) {
        return AUTH_ERR_WRONG_DOMAIN;
    }
    
    if ([dataStr rangeOfString:logoutMarker].location == NSNotFound) {
        
        if ([dataStr rangeOfString:verifyAccount].location != NSNotFound) {
            return AUTH_ERR_NOT_ACTIVATED;
        }
        
        return AUTH_ERR_WRONG_LOGIN;
    }
    
    return AUTH_OK;
}

+ (NSString *) getRankStr:(NSInteger)rank
{
    NSArray *ranks = @[@"Private soldier",
                       @"Junior sergeant",
                       @"Sergeant",
                       @"Senior sergeant",
                       @"Junior lieutenant",
                       @"Lieutenant",
                       @"Senior lieutenant",
                       @"Captain",
                       @"Major",
                       @"Lieutenant colonel",
                       @"Colonel",
                       @"Colonel-general",
                       @"Administrator",
                       @"Games Author",
                       @"Organizer",
                       @"Domain owner"];
    
    if (rank >= 0 && rank < [ranks count]) {
        return [ranks objectAtIndex:rank];
    }
    
    return @"Unknown";
}

- (NSNumber *) getRankId:(NSString *)rankStr
{
    NSDictionary *dic = @{@"Рядовой": [NSNumber numberWithInteger:1],
                                @"Младший сержант": [NSNumber numberWithInteger:2],
                                @"Сержант": [NSNumber numberWithInteger:3],
                                @"Старший сержант": [NSNumber numberWithInteger:4],
                                @"Младший лейтенант": [NSNumber numberWithInteger:5],
                                @"Лейтенант": [NSNumber numberWithInteger:6],
                                @"Старший лейтенант": [NSNumber numberWithInteger:7],
                                @"Капитан": [NSNumber numberWithInteger:8],
                                @"Майор": [NSNumber numberWithInteger:9],
                                @"Подполковник": [NSNumber numberWithInteger:10],
                                @"Полковник": [NSNumber numberWithInteger:11],
                                @"Генерал-полковник": [NSNumber numberWithInteger:12],
                                @"Администратор": [NSNumber numberWithInteger:13],
                                @"Автор игр": [NSNumber numberWithInteger:14],
                                @"Организатор": [NSNumber numberWithInteger:15],
                                @"Владелец домена": [NSNumber numberWithInteger:16],
                                };
    
    
    NSNumber *obj = [dic valueForKey:rankStr];
    if (!obj) obj = [NSNumber numberWithInt:1];
    return obj;
}

- (NSDate *) parseRusDate:(NSString *)dateStr
{
    NSDate *date = nil;
    
    NSDictionary *dic = @{@"января": @"01",
                          @"февраля": @"02",
                          @"марта": @"03",
                          @"апреля": @"04",
                          @"мая": @"05",
                          @"июня": @"06",
                          @"июля": @"07",
                          @"августа": @"08",
                          @"сентября": @"09",
                          @"октября": @"10",
                          @"ноября": @"11",
                          @"декабря": @"12",
                          };
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\s*(\\d{2})\\s*(.{3,8})\\s*(\\d{4})(\\s|$)"
                                                                           options:0
                                                                             error:NULL];
    NSTextCheckingResult *match = [regex firstMatchInString:dateStr
                                                    options:0
                                                      range:NSMakeRange(0, [dateStr length])];
    
    NSRange UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length == 0) {
        return nil;
    }
    
    NSString *month = [[dateStr substringWithRange:[match rangeAtIndex:2]] stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceCharacterSet]];
    
    NSString *dateStrNormal = [NSString stringWithFormat:@"%@.%@.%@", [dateStr substringWithRange:[match rangeAtIndex:1]], [dic valueForKey:month], [dateStr substringWithRange:[match rangeAtIndex:3]]];
    
    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc] init];
    [tempFormatter setDateFormat:@"dd.mm.yyyy"];
    
    date = [tempFormatter dateFromString:dateStrNormal];
    
    return date;
}

- (ENUser *) parseUserInfo:(NSData *)content
{
    ENUser *user = [[ENUser alloc] init];
    NSString *dataStr = [[NSString alloc] initWithData:content encoding:NSUTF8StringEncoding];
    
    NSRegularExpression *regex;
    NSTextCheckingResult *match;
    NSRange UserIdRange;
    
    //////////// Find user id
    regex = [NSRegularExpression regularExpressionWithPattern:@"var\\s*UserDetails_CurrentUserID\\s*=\\s*'(.*)'"
                                                                           options:0
                                                                             error:NULL];
    match = [regex firstMatchInString:dataStr
                                                    options:0
                                                      range:NSMakeRange(0, [dataStr length])];
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length == 0) {
        return nil;
    }
    user.userId = [[dataStr substringWithRange:UserIdRange] stringByTrimmingCharactersInSet:
    [NSCharacterSet whitespaceCharacterSet]];
    
    //////////// Find username
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLineAuthorization_authorizationInfoBlock_lblLogin.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length == 0) {
        return nil;
    }
    user.username = [[dataStr substringWithRange:UserIdRange] stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
    

    //////////// Find username
    regex = [NSRegularExpression regularExpressionWithPattern:@"enUserDetailsPanel_lnkDomain.*>(.*)</a>"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length == 0) {
        return nil;
    }
    
    NSString *address = [dataStr substringWithRange:UserIdRange];
    
    user.domain = [ENDomain domainWithAddress:[address stringByTrimmingCharactersInSet:
                                               [NSCharacterSet whitespaceCharacterSet]]];
    
    
    //////////// Find points
    regex = [NSRegularExpression regularExpressionWithPattern:@"enUserDetailsPanel_lblPointsVal.*>(.*)</span>"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length == 0) {
        return nil;
    }
    
    NSString *points = [[[dataStr substringWithRange:UserIdRange] stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""] stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    [f setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    user.points = [f numberFromString:points];

    
    //////////// Find rank
    regex = [NSRegularExpression regularExpressionWithPattern:@"UserRanks.aspx.*title\\s*=\\s*\"(.*)\".*</a>"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length == 0) {
        return nil;
    }
    
    NSString *rank = [[[dataStr substringWithRange:UserIdRange] stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]] stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    
    user.rank = [self getRankId:rank];
    

    //////////// Find team
    regex = [NSRegularExpression regularExpressionWithPattern:@"Статус:.*<span.{0,50}>(.*)</span>"//.*Teams/TeamDetails.aspx\\?tid=(.*)\">(.*)<"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    
    if (UserIdRange.length == 0) {
        return nil;
    }
    
    NSString *statusText = [dataStr substringWithRange:UserIdRange];
    
    if ([statusText hasPrefix:@"капитан команды"]) {
        regex = [NSRegularExpression regularExpressionWithPattern:@"Статус:.*Teams/TeamDetails.aspx\\?tid=(.*)\">(.*)</a>"
                                                          options:0
                                                            error:NULL];
        match = [regex firstMatchInString:dataStr
                                  options:0
                                    range:NSMakeRange(0, [dataStr length])];
        
        user.team = [ENTeam teamWithId:[dataStr substringWithRange:[match rangeAtIndex:1]]];
        user.team.name = [dataStr substringWithRange:[match rangeAtIndex:2]];
        user.isCapitan = YES;
        
    } else if ([statusText hasPrefix:@"в команде"]) {
        regex = [NSRegularExpression regularExpressionWithPattern:@"Статус:.*Teams/TeamDetails.aspx\\?tid=(.*)\">(.*)</a>"
                                                          options:0
                                                            error:NULL];
        match = [regex firstMatchInString:dataStr
                                  options:0
                                    range:NSMakeRange(0, [dataStr length])];
        
        user.team = [ENTeam teamWithId:[dataStr substringWithRange:[match rangeAtIndex:1]]];
        user.team.name = [dataStr substringWithRange:[match rangeAtIndex:2]];
        user.isCapitan = NO;
        
    } else {
        user.team = nil;
    }
    
    //////////// Find location
    regex = [NSRegularExpression regularExpressionWithPattern:@"enUserDetailsPanel_lnkDomain.*\\(*.>(.*)<.*\\)"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length == 0) {
        return nil;
    }
    
    user.location = [dataStr substringWithRange:UserIdRange];
    
    //////////// Find birthday
    regex = [NSRegularExpression regularExpressionWithPattern:@"Дата регистрации:\\s*.*>(.*)</span>"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length == 0) {
        return nil;
    }
    
    NSString *created = [dataStr substringWithRange:UserIdRange];
    
    user.created = [self parseRusDate:created];
 
    
    //////////// Find username
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLinePersonalData_personalDataBlock_lblGenderTextVal.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    
    if ([[dataStr substringWithRange:UserIdRange] isEqualToString:@"Женский"]) {
        user.gender = @"female";
    } else {
        user.gender = @"male";
    }
 
 
    
    //////////// Find firstName
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLinePersonalData_personalDataBlock_lblFirstNameVal.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length != 0 ) {
        user.firstName = [dataStr substringWithRange:UserIdRange];
    }
    
    //////////// Find patronymicName
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLinePersonalData_personalDataBlock_lblPatronymicNameVal.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length != 0 ) {
        user.patronymicName = [dataStr substringWithRange:UserIdRange];
    }
    
    //////////// Find lastName
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLinePersonalData_personalDataBlock_lblLastNameVal.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length != 0 ) {
        user.lastName = [dataStr substringWithRange:UserIdRange];
    }
 
    
    //////////// Find phone
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLineContacts_contactsBlock_lblMobilePhoneVal.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length != 0 ) {
        user.phone = [dataStr substringWithRange:UserIdRange];
    }
    
    //////////// Find email
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLineContacts_contactsBlock_lblEmailVal.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length != 0 ) {
        user.email = [dataStr substringWithRange:UserIdRange];
    }
    
    //////////// Find ICQ
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLineContacts_contactsBlock_ICQValue.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length != 0 ) {
        user.icq = [dataStr substringWithRange:UserIdRange];
    }

    
    //////////// Find picture
    regex = [NSRegularExpression regularExpressionWithPattern:@"enUserDetailsPanel_lnkAvatarEdit\">\\s*<img src=\"(.*)\" title"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length != 0 ) {
        user.pic = [dataStr substringWithRange:UserIdRange];
    }
    
    //////////// Find skype
    regex = [NSRegularExpression regularExpressionWithPattern:@"EnTabContainer1_content_ctl00_panelLineContacts_contactsBlock_SkypeValue.*>(.*)</span"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    UserIdRange = [match rangeAtIndex:1];
    if (UserIdRange.length != 0 ) {
        user.skype = [dataStr substringWithRange:UserIdRange];
    }

    return user;
}

- (NSInteger) getMessagesCount:(NSData *)content
{
    NSString *dataStr = [[NSString alloc] initWithData:content encoding:NSUTF8StringEncoding];
    
    NSRegularExpression *regex;
    NSTextCheckingResult *match;
    
    
    //////////// Find user id
    regex = [NSRegularExpression regularExpressionWithPattern:@"lnkMailIndikator\" href=\"/EnMail.aspx\">(\\d*)</a>"
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    NSString *str = [[dataStr substringWithRange:[match rangeAtIndex:1]] stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
    
    return [str integerValue];
}

- (NSString *) parseSignUp:(NSData *)content
{
    NSString *dataStr = [[NSString alloc] initWithData:content encoding:NSUTF8StringEncoding];
    
    NSRegularExpression *regex;
    NSTextCheckingResult *match;
    
    
    //////////// Find user id
    regex = [NSRegularExpression regularExpressionWithPattern:@"name=\"MagicGuid\".*value=\"(.{30,50})\" type="
                                                      options:0
                                                        error:NULL];
    match = [regex firstMatchInString:dataStr
                              options:0
                                range:NSMakeRange(0, [dataStr length])];
    
    NSString *str = [[dataStr substringWithRange:[match rangeAtIndex:1]] stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
    
    if (![str length]) {
        str = nil;
    }
    
    return str;
}

- (NSInteger) parseSignUpResult:(NSData *)content
{
    NSString *logoutMarker = @"id=\"RegistrationForm_lblErrMsg\">(.{0,500})</span></span></td>";
    NSString *dataStr = [[NSString alloc] initWithData:content encoding:NSUTF8StringEncoding];
    
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:logoutMarker
                                                          options:0
                                                            error:NULL];
    
    NSTextCheckingResult *match = [regex firstMatchInString:dataStr
                                                    options:0
                                                      range:NSMakeRange(0, [dataStr length])];
    
    NSRange range = [match rangeAtIndex:1];
    
    if(range.length == 0)
    {
        return ERR_SUCCESS;
    }
    
    dataStr = [dataStr substringWithRange:range] ;
    
    if ([dataStr rangeOfString:@"Введите ваш логин!"].location != NSNotFound) {
        return ERR_NO_LOGIN;
    }
    
    if ([dataStr rangeOfString:@"Введите ваш e-mail!"].location != NSNotFound) {
        return ERR_NO_EMAIL;
    }
    
    if ([dataStr rangeOfString:@"Введите пароль!"].location != NSNotFound) {
        return ERR_NO_PASSWORD;
    }
    
    if ([dataStr rangeOfString:@"Введите, пожалуйста, последовательность случайных цифр, изображенных на картинке"].location != NSNotFound) {
        return ERR_NO_CODE;
    }
    
    if ([dataStr rangeOfString:@"Указанный E-Mail не корректен!"].location != NSNotFound) {
        return ERR_WRONG_EMAIL;
    }
    
    if ([dataStr rangeOfString:@"Ваш пароль очень похож на логин!"].location != NSNotFound) {
        return ERR_WRONG_PASS_SAME;
    }
    
    if ([dataStr rangeOfString:@"Ваш пароль должен быть минимум 6 символов."].location != NSNotFound) {
        return ERR_WRONG_PASS_LENGTH;
    }
    
    if ([dataStr rangeOfString:@"Ваш пароль должен быть не больше 32 символов."].location != NSNotFound) {
        return ERR_WRONG_PASS_LENGTH;
    }
    
    if ([dataStr rangeOfString:@"Пароль должен содержать хотя бы одну букву и цифру."].location != NSNotFound) {
        return ERR_WRONG_PASS_SYMBOLS;
    }
    
    if ([dataStr rangeOfString:@"Введенные вами цифры не совпадают с нарисованными на картинке."].location != NSNotFound) {
        return ERR_WRONG_CODE;
    }
    
    if ([dataStr rangeOfString:@"уже существует, пожалуйста выберите другой логин."].location != NSNotFound) {
        return ERR_WRONG_LOGIN_EXISTS;
    }
    
    if ([dataStr rangeOfString:@"На указанный вами E-mail уже зарегистрирован"].location != NSNotFound) {
        return ERR_WRONG_EMAIL_EXISTS;
    }
    
    if ([dataStr rangeOfString:@"Ваш логин должен быть не более 32 символов."].location != NSNotFound) {
        return ERR_WRONG_LOGIN_LENGTH;
    }
    
    if ([dataStr rangeOfString:@"Ваш логин должен быть минимум 3 символа."].location != NSNotFound) {
        return ERR_WRONG_LOGIN_LENGTH;
    }
    
    return ERR_UNKNOWN;
}

- (BOOL) parseDomains:(NSData *)content complete:(void (^)(NSArray *sections, NSMutableDictionary *dic))complete
{
    NSString *dataStr = [[NSString alloc] initWithData:content encoding:NSUTF8StringEncoding];
    
    NSRegularExpression *regexName, *regexDomain;
    
    NSMutableArray *secs = [[NSMutableArray alloc] initWithCapacity:20];
    NSMutableDictionary *secdic = [[NSMutableDictionary alloc] initWithCapacity:50];
    
    NSString *countryName = @"#ctl\\d{2}_Geo_CountryRepeater_ctl\\d{2}_tr\">(.{1,50})</a></b>";
    NSString *countryDomain = @"ctl\\d{1,5}_Geo_CountryRepeater_ctl\\d{1,5}(_RegionsRepeater_ctl\\d{1,5})?_Region_SitesRepeater_ctl\\d{1,5}_geoSite_HlDomainOwner\"\\s+href=\"http://(.{3,100})/UserDetails.aspx\\?uid=\\d";
    
    regexName = [NSRegularExpression regularExpressionWithPattern:countryName
                                                      options:0
                                                        error:NULL];
    
    regexDomain = [NSRegularExpression regularExpressionWithPattern:countryDomain
                                                       options:0
                                                         error:NULL];
    
    NSArray *matches = [regexName matchesInString:dataStr options:0 range:NSMakeRange(0, [dataStr length])];

    for (NSUInteger i = 0; i < [matches count]; i++) {

        
        NSTextCheckingResult* res = [matches objectAtIndex:i];
        NSRange found = res.range;
        NSString *domainsContent = nil;
        
        if (([matches count] < 2) || (i+1 == [matches count])) {
            domainsContent = [dataStr substringFromIndex:found.location + found.length];
        } else {
            NSTextCheckingResult* res = [matches objectAtIndex:i+1];
            NSRange found2 = res.range;
            domainsContent = [dataStr substringToIndex:found2.location + found2.length];
            domainsContent = [domainsContent substringFromIndex:found.location + found.length];
        }
        
        NSString *countrStr = [dataStr substringWithRange:[res rangeAtIndex:1]];
        
        // Add to array list
        [secs addObject:countrStr];
        
//        NSLog(@"Country: %@", countrStr);
        
        NSArray *matches = [regexDomain matchesInString:domainsContent options:0 range:NSMakeRange(0, [domainsContent length])];
        
        NSMutableArray *domains = [[NSMutableArray alloc] initWithCapacity:30];
        
        for(NSTextCheckingResult* i in matches) {
            NSRange a = [i rangeAtIndex:2];
            NSString *domainStr = [domainsContent substringWithRange:a];
            
            [domains addObject:domainStr];
//            NSLog(@"Domain: %@", domainStr);
        }
        
        [domains sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [(NSString *)obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        
        [secdic setObject:domains forKey:countrStr];

    }
    
//    for(NSTextCheckingResult* i in matches) {
//        NSRange a = [i rangeAtIndex:1];
//        NSString *domainStr = [domainsContent substringWithRange:a];
//        [secdic setValue:domainStr forKey:countrStr];
//        NSLog(@"Domain: %@", domainStr);
//    }
    
//    while (found.length) {
//        // #ctl\d{2}_Geo_CountryRepeater_ctl\d{2}_tr">.{1,50}</a></b>
//        //ctl\d{1,5}_Geo_CountryRepeater_ctl\d{1,5}(_RegionsRepeater_ctl\d{1,5})?_Region_SitesRepeater_ctl\d{1,5}_geoSite_HlDomainOwner"\s*href="http://(.{3,50})/UserDetails.aspx\?uid=\d
//        
//    }
//    
    complete(secs, secdic);
    
    return YES;
}

@end
